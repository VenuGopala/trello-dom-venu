// Trello
const KEY="1b9d6e47b743207eb70276e3d4f8f212";
const TOKEN = "7fdc903f8116efad88d5dfade14ca22a3557d48224602b1dde2741cd213e446a";
const LIST_ID = "5ce61982a8645c3df3261364";
// const BOARD_ID = "hHig9kMJ";


let list=((KEY,TOKEN,LIST_ID)=>{
    return fetch(`https://api.trello.com/1/lists/${LIST_ID}/cards?fields=id&key=${KEY}&token=${TOKEN}`)
    .then(response => response.json())
    .then(item=>item.map(c=>(c.id)))
});

let checkItems=((cardId,KEY,TOKEN)=>{
    return fetch(`https://api.trello.com/1/cards/${cardId}/checklists?checkItems=all&checkItem_fields=name%2Cstate&filter=all&fields=idCard&key=${KEY}&token=${TOKEN}`)
    .then(response => response.json())
});

function appendHtmlCode(checkItems, idCard) {
    checkItems.map(checkItem => $('.cards').append(
        `<div class="checklistItem">
        <div id="div1"><input type="checkbox" class="check_boxes" data-id='${checkItem.id}' data-cardId='${idCard}' ${checkItem.state=='complete'?"checked":null }></div>
        <div class=${checkItem.state=='complete'?"strike":null } data-id='${checkItem.id}' data-cardId='${idCard}'><p id="checkItem" > ${checkItem.name} </p></div>
        <div id="delete_icon" data-id='${checkItem.id}' data-cardId='${idCard}'><i class="fa fa-trash-o" ></i></div>
        </div>`
    ))
}

list(KEY,TOKEN,LIST_ID).then(cardIds=>Promise.all(cardIds.map((i)=>checkItems(i,KEY,TOKEN))))
.then(card=>card.map(checklist => checklist.map(checkItem=> appendHtmlCode(checkItem.checkItems,checkItem.idCard))))

$(document).on('click','.check_boxes', function () {
    let checkItemId = $(this).attr('data-id');
    let cardId = $(this).attr('data-cardId');
    let state = this.checked ? 'complete' : 'incomplete';
    fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=${state}&key=${KEY}&token=${TOKEN}`, {
        'method': 'PUT'})
    .then(()=>{
    this.checked ? $(this).parent().siblings("#checkItem").attr('class','strike') : $(this).parent().siblings("#checkItem").attr('class',null)
    }).catch(error => console.log(error));
    console.log("done");
});

$(document).on('click', '#delete_icon', function () {
    let checkItemId = $(this).attr('data-id');
    let cardId = $(this).attr('data-cardId');
    fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${KEY}&token=${TOKEN}`, {
        method: 'DELETE'})
    .then(()=>{
    $(this).parent().remove()
    })
    .catch(error => console.log(error)); 
});

$(document).on('click','button',function(){
   let temp;
    $(this).siblings().append(`<input id="input_new" type="text" value=""></input>`)
     $("#input_new").focusout(function(){
     temp=$(this).val();
     fetch(`https://api.trello.com/1/checklists/5d0cb6092de6575bb2f127ce/checkItems?name=${temp}&pos=bottom&checked=false&key=${KEY}&token=${TOKEN}`,{
        method: 'POST'}).catch(error => console.log(error)); 
        location.reload();
     })
});
$(document).on('dblclick','#checkItem',function(){
    let temp1=JSON.stringify($(this).text());
    let checkItemId = $(this).parent().attr('data-id');
    let cardId = $(this).parent().attr('data-cardId');
    $(this).append(`<input type="text" id="edit" value = ${temp1}></input> <input type="button" value="submit" id="submit">`)
     $('#edit').focusout(function () {
        let temp=$(this).val();
        fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?name=${temp}&key=${KEY}&token=${TOKEN}`,{
            method: 'PUT'})
            .catch(error => console.log(error));
        $('#submit').dblclick(function(){
            location.reload();
        })
    })
});    
